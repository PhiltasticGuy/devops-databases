$liquibaseInstallDir = "./liquibase/"
$liquibaseExe = $liquibaseInstallDir + "liquibase"
$postgresJdbcJar = $liquibaseInstallDir + "postgresql.jar"

# Read user inputs.
$dbHostDefault = "localhost"
if (!($dbHost = Read-Host "Enter database host [$dbHostDefault]")) { $dbHost = $dbHostDefault }

$dbPortDefault = 5432
if (!($dbPort = Read-Host "Enter database port [$dbPortDefault]")) { $dbPort = $dbPortDefault }

$dbNameDefault = "addressbook"
if (!($dbName = Read-Host "Enter database name [$dbNameDefault]")) { $dbName = $dbNameDefault }

$dbUsrDefault = "root"
if (!($dbUsr = Read-Host "Enter database username [$dbUsrDefault]")) { $dbUsr = $dbUsrDefault }

$dbPwdDefault = "mysecretpassword"
$dbPwd = Read-Host "Enter database password [$dbPwdDefault]" -AsSecureString
if ($dbPwd.Length -eq 0) { $dbPwd = ConvertTo-SecureString $dbPwdDefault -AsPlainText -Force }

# Ensure that JAVA_HOME is set!
if (!$Env:JAVA_HOME)
{ 
	Write-Host "# Liquibase depends on the JAVA_HOME environment variable being set."
	Write-Host "# Please set JAVA_HOME to the root of your JDK installation."
}
else
{
	# Run Liquibase update.
    $params = @(
        "--driver=org.postgresql.Driver"
        "--classpath=" + $postgresJdbcJar
        "--url=""jdbc:postgresql://${dbHost}:${dbPort}/${dbName}"""
        "--changeLogFile=""db/changelogs/db.changelog-master.xml"""
        "--username=" + $dbUsr
        "--password=" + [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($dbPwd))
        "--logLevel=info"
        "update"
    )
    Start-Process $liquibaseExe -ArgumentList $params -Wait -NoNewWindow
}