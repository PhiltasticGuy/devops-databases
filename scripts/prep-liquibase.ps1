$liquibaseReleaseUrl = "https://github.com/liquibase/liquibase/releases/download/v3.10.0/liquibase-3.10.0.zip"
$liquibaseInstallDir = "./liquibase/"
$liquibaseZip = $liquibaseInstallDir + "liquibase.zip"
$liquibaseExe = $liquibaseInstallDir + "liquibase"

$postgresJdbcReleaseUrl = "https://jdbc.postgresql.org/download/postgresql-42.2.14.jar"
$postgresJdbcJar = $liquibaseInstallDir + "postgresql.jar"

# Fetch/Extract Liquibase.
Invoke-WebRequest -uri $liquibaseReleaseUrl -OutFile ( New-Item -Path $liquibaseZip -Force )
Expand-Archive -Path $liquibaseZip -DestinationPath $liquibaseInstallDir -Force
Remove-Item -Path $liquibaseZip

# Fetch PostgreSQL JDBC library.
Invoke-WebRequest -uri $postgresJdbcReleaseUrl -OutFile ( New-Item -Path $postgresJdbcJar -Force )

# Ensure that JAVA_HOME is set!
if (!$Env:JAVA_HOME)
{ 
	Write-Host "# Liquibase depends on the JAVA_HOME environment variable being set."
	Write-Host "# Please set JAVA_HOME to the root of your JDK installation."
}
else
{
	# Run Liquibase as installation validation.
	& $liquibaseExe --version
}