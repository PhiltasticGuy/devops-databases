# DevOps for Databases - Liquibase

A simple demo application to show how Liquibase can be used to manage your database changes while also storing them in source control.

## Technologies Used

* .NET Core
* ASP.NET Core - Razor Pages
* EntityFramework Core
* PostgreSQL
* Liquibase
* Docker
* PowerShell

## Getting Started

### Prerequisites

* Java Runtime Environment (JRE): Download and run the installer from [AdoptOpenJDK's website](https://adoptopenjdk.net/installation.html#installers).
* Docker Desktop: Download and install the Docker Desktop straight from [Docker's website](https://www.docker.com/products/docker-desktop).

### Configurations

For our Docker Compose setup, the configurations are read from an `.env` file placed at the root of the project directory. 
Simply create a copy of the `.env.example` file and rename it to `.env`.

Here are the available configurations in the `.env.example` file.

```INI
DB_HOST=db
DB_PORT=5432
DB_DATABASE=addressbook
DB_USERNAME=root
DB_PASSWORD=mysecretpassword
```

**Please note that if you change the value of DB_DATABASE you will also need to update it in the `./db/sql/init/init.db` file.**

### Run docker-compose

```Batchfile
docker-compose up -d
```

### Run ./scripts/prep-liquibase.ps1

```Batchfile
.\scripts\prep-liquibase.ps1
```

### Run ./scripts/run-liquibase.ps1

```Batchfile
.\scripts\run-liquibase.ps1
```

### Open in Browser

The web application should be accessible in your browser from <https://localhost:9000/>.
