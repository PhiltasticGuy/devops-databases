--liquibase formatted sql
--changeset author-name:table_contacts_seed

INSERT INTO contacts (
    id,
    first_name,
    last_name,
    email,
    phone
)
VALUES
(
    'F8D8B98F-1B8A-4146-8002-E91BA6A8B178',
    'John',
    'Smith',
    'john.smith@example.com',
    '408-237-2345'
),
(
    '4E424E48-6357-4714-92B2-5C76A70812D5',
    'Jane',
    'Smith',
    'jane.smith@example.com',
    '408-237-2344'
),
(
    '9FB0F54E-1689-497A-BFF7-E4E1555427CF',
    'Alex',
    'Smith',
    'alex.smith@example.com',
    '408-237-2343'
);