--liquibase formatted sql
--changeset author-name:table_contacts_v1

CREATE TABLE contacts (
    id uuid NOT NULL,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    phone VARCHAR,
    PRIMARY KEY (id)
);