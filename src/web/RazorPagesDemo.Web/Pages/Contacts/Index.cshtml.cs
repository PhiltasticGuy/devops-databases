﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RazorPagesDemo.Web.Data;
using RazorPagesDemo.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RazorPagesDemo.Web.Pages.Contacts
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IContactRepository _contactRepository;

        public IEnumerable<Contact> GetContactsResult { get; set; }

        public bool DatabaseError { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IContactRepository contactRepository)
        {
            _logger = logger;
            _contactRepository = contactRepository;
        }

        public void OnGet()
        {
            GetContacts();
        }

        private void GetContacts()
        {
            var contacts = _contactRepository.GetContacts();

            if (contacts?.Any() == true)
            {
                GetContactsResult = contacts;
            }
            else
            {
                GetContactsResult = Array.Empty<Contact>();
            }
        }
    }
}