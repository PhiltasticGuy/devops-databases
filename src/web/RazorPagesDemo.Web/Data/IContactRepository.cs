﻿using RazorPagesDemo.Web.Models;
using System.Collections.Generic;

namespace RazorPagesDemo.Web.Data
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetContacts();
    }
}
