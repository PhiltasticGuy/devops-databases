﻿using RazorPagesDemo.Web.Models;
using System.Collections.Generic;

namespace RazorPagesDemo.Web.Data
{
    public class ContactRepository : IContactRepository
    {
        private readonly AddressBookDbContext _addressBookDbContext;

        public ContactRepository(AddressBookDbContext addressBookDbContext)
        {
            _addressBookDbContext = addressBookDbContext;
        }

        public IEnumerable<Contact> GetContacts()
        {
            return _addressBookDbContext.Contacts;
        }
    }
}
